-- Create Address table
CREATE TABLE address (
    id INT AUTO_INCREMENT PRIMARY KEY,
    street VARCHAR(255),
    city VARCHAR(255),
    province VARCHAR(255),
    country VARCHAR(255),
    zipCode VARCHAR(20)
);

-- Create Manufacturer table
CREATE TABLE manufacturer (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    address_id INT,
    FOREIGN KEY (address_id) REFERENCES Address(id)
);

-- Create Brand table
CREATE TABLE brand (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
);

-- Create Product table
CREATE TABLE Product (
    id INT AUTO_INCREMENT PRIMARY KEY,
    artNumber VARCHAR(50),
    name VARCHAR(255),
    description TEXT,
    stock INT,
    manufacturer_id INT,
    brand_id INT,
    FOREIGN KEY (manufacturer_id) REFERENCES manufacturer(id),
    FOREIGN KEY (brand_id) REFERENCES brand(id)
);

-- Create Valuta table
CREATE TABLE valuta (
    id INT AUTO_INCREMENT PRIMARY KEY,
    code VARCHAR(10),
    name VARCHAR(255)
);

-- Create Price table
CREATE TABLE price (
    id INT AUTO_INCREMENT PRIMARY KEY,
    amount DECIMAL(10,6),
    product_id INT,
    valuta_id INT,
    FOREIGN KEY (product_id) REFERENCES product(id),
    FOREIGN KEY (valuta_id) REFERENCES valuta(id)
);
