SELECT
    P.id AS product_id,
    P.artNumber,
    P.name AS product_name,
    P.description,
    P.stock,
    M.name AS manufacturer_name,
    B.name AS brand_name,
    A.street AS address_street,
    A.city AS address_city,
    A.province AS address_province,
    A.country AS address_country,
    A.zipCode AS address_zipCode,
    PR.amount AS price_amount,
    V.code AS valuta_code,
    V.name AS valuta_name
FROM
    product P
JOIN
    manufacturer M ON P.manufacturer_id = M.id
JOIN
    brand B ON P.brand_id = B.id
JOIN
    address A ON M.address_id = A.id
JOIN
    price PR ON P.id = PR.product_id
JOIN
    valuta V ON PR.valuta_id = V.id
WHERE
    P.stock = 0;
