-- Insert data into Address table
INSERT INTO address (street, city, province, country, zipCode) VALUES
('123 Main St', 'City1', 'Province1', 'Country1', '12345'),
('456 Oak St', 'City2', 'Province2', 'Country2', '67890');

-- Insert data into Manufacturer table
INSERT INTO manufacturer (name, address_id) VALUES
('Manufacturer1', 1),
('Manufacturer2', 2);

-- Insert data into Brand table
INSERT INTO brand (name) VALUES
('Brand1'),
('Brand2');

-- Insert data into Product table
INSERT INTO product (artNumber, name, description, stock, manufacturer_id, brand_id) VALUES
('A123', 'Product1', 'Description1', 10, 1, 1),
('B456', 'Product2', 'Description2', 5, 2, 1),
('C789', 'Product3', 'Description3', 8, 1, 2),
('D101', 'Product4', 'Description4', 0, 2, 2),
('E112', 'Product5', 'Description5', 12, 1, 1),
('F345', 'Product6', 'Description6', 0, 2, 1),
('G678', 'Product7', 'Description7', 15, 1, 2),
('H910', 'Product8', 'Description8', 3, 2, 2),
('I111', 'Product9', 'Description9', 20, 1, 1),
('J121', 'Product10', 'Description10', 0, 2, 2);

-- Insert data into Valuta table
INSERT INTO valuta (code, name) VALUES
('USD', 'US Dollar'),
('SGD', 'Singapore Dollar');

-- Insert data into Price table
INSERT INTO price (amount, product_id, valuta_id) VALUES
(50.99, 1, 1),
(30.50, 2, 2),
(25.75, 3, 1),
(10.00, 4, 2),
(15.99, 5, 1),
(8.75, 6, 2),
(40.00, 7, 1),
(22.50, 8, 2),
(18.99, 9, 1),
(12.25, 10, 2),
(45.00, 1, 2),
(27.50, 2, 1),
(22.75, 3, 2),
(9.99, 4, 1),
(14.75, 5, 2),
(7.00, 6, 1),
(35.00, 7, 2),
(20.50, 8, 1),
(16.99, 9, 2),
(11.25, 10, 1);
