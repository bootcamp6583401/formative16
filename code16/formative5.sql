SELECT
    p.id AS product_id,
    p.artNumber,
    p.name AS product_name,
    p.description,
    SUM(p.stock) AS total_stock,
    m.name AS manufacturer_name,
    b.name AS brand_name,
    -- a.street AS address_street,
    -- a.city AS address_city,
    -- a.province AS address_province,
    -- a.country AS address_country,
    -- a.zipCode AS address_zipCode,
    SUM(pr.amount) AS price_amount,
    v.code AS valuta_code,
    v.name AS valuta_name
FROM
    product p
JOIN
    manufacturer m ON p.manufacturer_id = m.id
JOIN
    brand b ON p.brand_id = b.id
JOIN
    address a ON m.address_id = a.id
JOIN
    price pr ON p.id = pr.product_id
JOIN
    valuta v ON pr.valuta_id = v.id
GROUP BY
    v.id, p.id
ORDER BY
    price_amount DESC;
