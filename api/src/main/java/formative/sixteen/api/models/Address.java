package formative.sixteen.api.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.Data;

@Entity
@Data
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String street;

    private String city;

    private String province;

    private String country;

    @Column(name = "zipcode")
    private String zipCode;

    @OneToOne(mappedBy = "address")
    @JsonManagedReference(value = "manufacturer-address")
    private Manufacturer manufacturer;

}
