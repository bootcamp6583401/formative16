package formative.sixteen.api.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.sixteen.api.models.Brand;

import java.util.List;

public interface BrandRepository extends CrudRepository<Brand, Integer> {
    @Query(value = "SELECT * FROM brand order by name", nativeQuery = true)
    public List<Brand> getAllSortByName();
}
