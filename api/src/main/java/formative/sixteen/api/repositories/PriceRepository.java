package formative.sixteen.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.sixteen.api.models.Price;

public interface PriceRepository extends CrudRepository<Price, Integer> {
    @Query(value = "SELECT * FROM price order by amount", nativeQuery = true)
    public List<Price> getAllSortByAmount();
}
