package formative.sixteen.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.sixteen.api.models.Valuta;

public interface ValutaRepository extends CrudRepository<Valuta, Integer> {
    @Query(value = "SELECT * FROM valuta order by name", nativeQuery = true)
    public List<Valuta> getAllSortByName();

    @Query(value = "SELECT * FROM valuta order by code", nativeQuery = true)
    public List<Valuta> getAllSortByCode();
}
