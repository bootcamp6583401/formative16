package formative.sixteen.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.sixteen.api.models.Address;

public interface AddressRepository extends CrudRepository<Address, Integer> {
    @Query(value = "SELECT * FROM address order by street", nativeQuery = true)
    public List<Address> getAllSortByStreetName();
}
