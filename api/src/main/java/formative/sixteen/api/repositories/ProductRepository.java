package formative.sixteen.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.sixteen.api.models.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {

    @Query(value = "SELECT * FROM product where stock <= 0", nativeQuery = true)
    public List<Product> getAllOutOfStock();

    @Query(value = "SELECT * FROM product order by stock DESC", nativeQuery = true)
    public List<Product> getAllSortByStock();
}
