package formative.sixteen.api.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.sixteen.api.models.Manufacturer;

import java.util.List;

public interface ManufacturerRepository extends CrudRepository<Manufacturer, Integer> {
    @Query(value = "SELECT * FROM manufacturer order by name", nativeQuery = true)
    public List<Manufacturer> getAllSortByName();
}
