package formative.sixteen.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import formative.sixteen.api.models.Brand;
import formative.sixteen.api.services.impl.BrandServiceImpl;

@RestController
@RequestMapping("/brands")
public class BrandController {

    @Autowired
    BrandServiceImpl service;

    @GetMapping
    public Iterable<Brand> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Brand save(@RequestBody Brand product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public Brand getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Brand updateById(@PathVariable int id, @RequestBody Brand product) {
        return service.updateById(id, product);
    }

}