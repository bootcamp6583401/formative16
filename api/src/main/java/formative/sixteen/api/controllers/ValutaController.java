package formative.sixteen.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import formative.sixteen.api.models.Valuta;
import formative.sixteen.api.services.impl.ValutaServiceImpl;

@RestController
@RequestMapping("/valutas")
public class ValutaController {

    @Autowired
    ValutaServiceImpl service;

    @GetMapping
    public Iterable<Valuta> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Valuta save(@RequestBody Valuta product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public Valuta getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Valuta updateById(@PathVariable int id, @RequestBody Valuta product) {
        return service.updateById(id, product);
    }

}