package formative.sixteen.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import formative.sixteen.api.models.Address;
import formative.sixteen.api.services.impl.AddressServiceImpl;

@RestController
@RequestMapping("/addresses")
public class AddressController {

    @Autowired
    AddressServiceImpl service;

    @GetMapping
    public Iterable<Address> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Address save(@RequestBody Address product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public Address getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Address updateById(@PathVariable int id, @RequestBody Address product) {
        return service.updateById(id, product);
    }

}