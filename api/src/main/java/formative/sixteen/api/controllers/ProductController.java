package formative.sixteen.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import formative.sixteen.api.models.Product;
import formative.sixteen.api.request.ProductRequest;
import formative.sixteen.api.services.impl.ProductServiceImpl;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    ProductServiceImpl service;

    @GetMapping
    public Iterable<Product> getAll() {
        return service.getAll();
    }

    @GetMapping("/out")
    public Iterable<Product> getAllOutOfStock() {
        return service.getAllOutOfStock();
    }

    @PostMapping
    public Product save(@RequestBody ProductRequest req) {
        return service.complexSave(req);
    }

    @GetMapping("/{id}")
    public Product getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Product updateById(@PathVariable int id, @RequestBody Product product) {
        return service.updateById(id, product);
    }

}