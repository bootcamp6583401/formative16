package formative.sixteen.api.request;

import formative.sixteen.api.models.Product;
import lombok.Data;

@Data
public class ProductRequest {
    private Product product;
    private int brand_id;
    private int manufacturer_id;

    public ProductRequest(Product product, int brand_id, int manufacturer_id) {
        this.product = product;
        this.brand_id = brand_id;
        this.manufacturer_id = manufacturer_id;
    }

}
