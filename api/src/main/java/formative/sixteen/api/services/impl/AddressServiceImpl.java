package formative.sixteen.api.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.sixteen.api.models.Address;
import formative.sixteen.api.repositories.AddressRepository;
import jakarta.persistence.EntityNotFoundException;

@Service
public class AddressServiceImpl {

    @Autowired
    AddressRepository repo;

    public Iterable<Address> getAll() {
        return repo.findAll();
    }

    public Address save(Address address) {
        return repo.save(address);
    }

    public Address getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Address not found"));
    }

    public Address updateById(int id, Address address) {
        Optional<Address> res = repo.findById(id);

        if (res.isPresent()) {
            address.setId(id);
            return repo.save(address);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}