package formative.sixteen.api.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.sixteen.api.models.Manufacturer;
import formative.sixteen.api.repositories.ManufacturerRepository;
import jakarta.persistence.EntityNotFoundException;

@Service
public class ManufacturerServiceImpl {

    @Autowired
    ManufacturerRepository repo;

    public Iterable<Manufacturer> getAll() {
        return repo.findAll();
    }

    public Manufacturer save(Manufacturer manufacturer) {
        return repo.save(manufacturer);
    }

    public Manufacturer getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Manufacturer not found"));
    }

    public Manufacturer updateById(int id, Manufacturer manufacturer) {
        Optional<Manufacturer> res = repo.findById(id);

        if (res.isPresent()) {
            manufacturer.setId(id);
            return repo.save(manufacturer);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}