package formative.sixteen.api.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.sixteen.api.models.Valuta;
import formative.sixteen.api.repositories.ValutaRepository;
import jakarta.persistence.EntityNotFoundException;

@Service
public class ValutaServiceImpl {

    @Autowired
    ValutaRepository repo;

    public Iterable<Valuta> getAll() {
        return repo.findAll();
    }

    public Valuta save(Valuta valuta) {
        return repo.save(valuta);
    }

    public Valuta getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Valuta not found"));
    }

    public Valuta updateById(int id, Valuta valuta) {
        Optional<Valuta> res = repo.findById(id);

        if (res.isPresent()) {
            valuta.setId(id);
            return repo.save(valuta);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}