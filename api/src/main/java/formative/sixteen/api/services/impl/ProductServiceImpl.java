package formative.sixteen.api.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.sixteen.api.models.Brand;
import formative.sixteen.api.models.Manufacturer;
import formative.sixteen.api.models.Product;
import formative.sixteen.api.repositories.ProductRepository;
import formative.sixteen.api.request.ProductRequest;
import jakarta.persistence.EntityNotFoundException;

@Service
public class ProductServiceImpl {

    @Autowired
    ProductRepository repo;

    @Autowired
    BrandServiceImpl brandSer;

    @Autowired
    ManufacturerServiceImpl manuSer;

    public Iterable<Product> getAll() {
        return repo.findAll();
    }

    public Product save(Product product) {
        return repo.save(product);
    }

    public Product getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Product not found"));
    }

    public Product updateById(int id, Product product) {
        Optional<Product> res = repo.findById(id);

        if (res.isPresent()) {
            product.setId(id);
            return repo.save(product);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

    public List<Product> getAllOutOfStock() {
        return repo.getAllOutOfStock();
    }

    public Product complexSave(ProductRequest req) {
        Product product = save(req.getProduct());

        Brand brand = brandSer.getById(req.getBrand_id());
        Manufacturer manufacturer = manuSer.getById(req.getManufacturer_id());

        product.setBrand(brand);
        product.setManufacturer(manufacturer);

        return repo.save(product);
    }

}