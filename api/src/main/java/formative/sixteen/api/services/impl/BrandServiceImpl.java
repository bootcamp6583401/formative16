package formative.sixteen.api.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.sixteen.api.models.Brand;
import formative.sixteen.api.repositories.BrandRepository;
import jakarta.persistence.EntityNotFoundException;

@Service
public class BrandServiceImpl {

    @Autowired
    BrandRepository repo;

    public Iterable<Brand> getAll() {
        return repo.findAll();
    }

    public Brand save(Brand brand) {
        return repo.save(brand);
    }

    public Brand getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Brand not found"));
    }

    public Brand updateById(int id, Brand brand) {
        Optional<Brand> res = repo.findById(id);

        if (res.isPresent()) {
            brand.setId(id);
            return repo.save(brand);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}