package formative.sixteen.api.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.sixteen.api.models.Price;
import formative.sixteen.api.repositories.PriceRepository;
import jakarta.persistence.EntityNotFoundException;

@Service
public class PriceServiceImpl {

    @Autowired
    PriceRepository repo;

    public Iterable<Price> getAll() {
        return repo.findAll();
    }

    public Price save(Price price) {
        return repo.save(price);
    }

    public Price getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Price not found"));
    }

    public Price updateById(int id, Price price) {
        Optional<Price> res = repo.findById(id);

        if (res.isPresent()) {
            price.setId(id);
            return repo.save(price);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}